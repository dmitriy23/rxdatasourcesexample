//
//  CollectionViewController.swift
//  RxDataSourcesExample
//
//  Created by Dmitriy Demchenko on 11/10/16.
//  Copyright © 2016 Dmitriy Demchenko. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class CollectionViewController: UIViewController {
    
    @IBOutlet private weak var addBarButtonItem: UIBarButtonItem!
    @IBOutlet private weak var collectionView: UICollectionView!
    @IBOutlet private var longPressGestureRecognizer: UILongPressGestureRecognizer!
    
    let disposeBag = DisposeBag()
    let dataSource = RxCollectionViewSectionedAnimatedDataSource<AnimatedSectionModel>()
    
    let data = Variable([
        AnimatedSectionModel(title: "Section: 0", data: ["0-0"])
        ])
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // data source

        dataSource.configureCell = { _, collectionView, indexPath, title in
            let cell = collectionView.dequeueReusableCell(
                withReuseIdentifier: "CollectionViewCell",
                for: indexPath) as! CollectionViewCell
            cell.titleLabel.text = title
            return cell
        }
        
        // supplementary view
        
        dataSource.supplementaryViewFactory = { dataSource, collectionView, kind, indexPath in
            let header = collectionView.dequeueReusableSupplementaryView(
                ofKind: kind,
                withReuseIdentifier: "CollectionHeaderView",
                for: indexPath
            ) as! CollectionHeaderView
            header.titleLabel.text = "Section: \(self.data.value.count)"
            return header
        }
        
        data.asDriver().drive(collectionView.rx.items(dataSource: dataSource)).addDisposableTo(disposeBag)
        
        // add new items
        
        addBarButtonItem.rx.tap.asDriver().drive(onNext: {
            let section = self.data.value.count
            let items: [String] = {
                var tempItems = [String]()
                let random = Int(arc4random_uniform(5)) + 1
                (0...random).forEach {
                    tempItems.append("\(section)-\($0)")
                }
                return tempItems
            }()
            self.data.value += [AnimatedSectionModel(title: "Section: \(section)", data: items)]
        }).addDisposableTo(disposeBag)
        
        // long press
        
        longPressGestureRecognizer.rx.event.subscribe(onNext: {
            switch $0.state {
            case .began:
                guard let selectedIndexPath = self.collectionView.indexPathForItem(at: $0.location(in: self.collectionView)) else {
                    break
                }
                self.collectionView.beginInteractiveMovementForItem(at: selectedIndexPath)
            case .changed:
                self.collectionView.updateInteractiveMovementTargetPosition( $0.location(in: self.view!))
            case .ended:
                self.collectionView.endInteractiveMovement()
            default:
                self.collectionView.cancelInteractiveMovement()
            }
        }).addDisposableTo(disposeBag)
    }
    
    deinit {
        print("DEinited!")
    }

}
