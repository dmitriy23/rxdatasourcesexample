//
//  AnimatedSectionModel.swift
//  RxDataSourcesExample
//
//  Created by Dmitriy Demchenko on 11/10/16.
//  Copyright © 2016 Dmitriy Demchenko. All rights reserved.
//

import RxSwift
import RxCocoa
import RxDataSources

extension String {
    public typealias Identity = String
    public var identity: Identity { return self }
}

struct AnimatedSectionModel {
    let title: String
    var data: [String]
}

extension AnimatedSectionModel: AnimatableSectionModelType {
    typealias Item = String
    typealias Identity = String
    
    var identity: Identity { return title }
    var items: [Item] { return data }
    
    init(original: AnimatedSectionModel, items: [String]) {
        self = original
        data = items
    }
}
