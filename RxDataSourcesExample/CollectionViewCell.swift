//
//  CollectionViewCell.swift
//  RxDataSourcesExample
//
//  Created by Dmitriy Demchenko on 11/10/16.
//  Copyright © 2016 Dmitriy Demchenko. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    
}
