//
//  Food.swift
//  RxDataSourcesExample
//
//  Created by Dmitriy Demchenko on 11/10/16.
//  Copyright © 2016 Dmitriy Demchenko. All rights reserved.
//

import UIKit

struct Food {
    let name: String
    let flickrId: String
    var image: UIImage?
    
    init(name: String, flickrId: String, image: UIImage? = nil) {
        self.name = name
        self.flickrId = flickrId
        self.image = UIImage(named: flickrId)
    }
    
}

extension Food: CustomStringConvertible {
    
    var description: String {
        return "\(name): flickrId.com/\(flickrId)"
    }
    
}
