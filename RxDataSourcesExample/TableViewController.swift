//
//  TableViewController.swift
//  RxDataSourcesExample
//
//  Created by Dmitriy Demchenko on 11/10/16.
//  Copyright © 2016 Dmitriy Demchenko. All rights reserved.
//

import UIKit
import RxSwift
import RxCocoa
import RxDataSources

class TableViewController: UIViewController {

    @IBOutlet private weak var tableView: UITableView!
    
    let foods = Observable.just([
        Food(name: "Banana", flickrId: "Banana"),
        Food(name: "Carrot", flickrId: "Carrot"),
        Food(name: "Grapes", flickrId: "Grapes"),
        Food(name: "Pepper", flickrId: "Pepper"),
        Food(name: "Pineapple", flickrId: "Pineapple")
    ])
    
    let disposeBag = DisposeBag()
    
    // MARK: - Life cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        foods.bindTo(tableView.rx.items(cellIdentifier: "Cell")) { row, food, cell in
            cell.textLabel?.text = food.name
            cell.detailTextLabel?.text = food.flickrId
            cell.imageView?.image = food.image
            }.addDisposableTo(disposeBag)
        
        tableView.rx.modelSelected(Food.self).subscribe(onNext: {
            print("You selected \($0)")
        }).addDisposableTo(disposeBag)
    }
    
}
